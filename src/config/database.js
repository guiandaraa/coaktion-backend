const mongoose = require('mongoose');
const dotenv = require('dotenv');

dotenv.config({ path: '.env' });
const url = process.env.DATABASE_URL;

mongoose.connect(url, { useNewUrlParser: true , useUnifiedTopology: true }, () =>{
    console.log('DATABASE ONLINE');
});

module.exports = mongoose;