const mongoose = require('../config/database');
const Schema = mongoose.Schema;

const BookSchema = new Schema({
    title: { type: String, required: true },
    author: { type: String, required: true },
    description: { type: String, required: true },
    created_at: { type: Date, default: Date.now() }
});

module.exports = mongoose.model('Book', BookSchema);