const mongoose = require('../config/database');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    name: { type: String, required: true },
    password: { type: String, required: true, select: false },
    email: { type: String, required: true },
    role: { type: String, enum: ['admin', 'user'], required: true, default: 'user' },
    created_at: { type: Date, default: Date.now() }
});

module.exports = mongoose.model('User', UserSchema);