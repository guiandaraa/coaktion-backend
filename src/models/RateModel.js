const mongoose = require('../config/database');
const Schema = mongoose.Schema;

const RateSchema = new Schema({
    book_id: { type: String, required: true },
    user_id: { type: String, required: true },
    rate: { type: String, required: true },
    comment: { type: String, required: true },
    created_at: { type: Date, default: Date.now() }
});

module.exports = mongoose.model('Rate', RateSchema);