const express = require('express');
const cors = require('cors');

const UserRoutes = require('./routes/UserRoutes');
const BookRoutes = require('./routes/BookRoutes');
const RateRoutes = require('./routes/RateRoutes');


const server = express();
server.use(cors());
server.use(express.json());


server.use('/user' , UserRoutes);
server.use('/book' , BookRoutes);
server.use('/rate' , RateRoutes);


server.listen(3333, () => {
    console.log('API ONLINE');
})

