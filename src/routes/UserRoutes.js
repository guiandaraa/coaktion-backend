const express = require('express');
const router = express.Router();

const UserController = require('../controller/UserController');


router.post('/login', UserController.login);
router.post('/', UserController.create);
router.delete('/:id', UserController.delete);

module.exports = router;
