const express = require('express');
const router = express.Router();

const BookController = require('../controller/BookController');


router.post('/', BookController.create);
router.get('/', BookController.findAll);
router.get('/:id', BookController.findById);
router.delete('/:id', BookController.delete);
router.put('/:id', BookController.update);


module.exports = router;
