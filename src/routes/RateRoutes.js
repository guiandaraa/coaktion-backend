const express = require('express');
const router = express.Router();

const RateController = require('../controller/RateController');


router.post('/', RateController.create);
router.get('/book/:id', RateController.findByBookId);
router.get('/user/:id', RateController.findByUserId);

module.exports = router;
