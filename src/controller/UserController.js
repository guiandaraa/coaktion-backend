const UserModel = require('../models/UserModel');

class UserController {

    async login(req, res) {
        await UserModel.findOne({ 'email': req.body.email, 'password': req.body.password })
            .then(response => {
                return res.status(200).json(response);
            })
            .catch(err => {
                return res.status(500).json(err);
            })
    }

    async create(req, res) {
        const task = new UserModel(req.body);
        await task
            .save()
            .then(response => {
                return res.status(200).json(response);
            })
            .catch(error => {
                return res.status(500).json(error);
            })
    }

    async delete(req, res) {
        await UserModel.deleteOne({ '_id': req.params.id })
            .then(response => {
                return res.status(200).json(response);
            })
            .catch(err => {
                return res.status(500).json(err);
            })
    }
}

module.exports = new UserController();