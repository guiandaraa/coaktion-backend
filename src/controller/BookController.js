const BookModel = require('../models/BookModel');

class BookController {

    async create(req, res) {
        const task = new BookModel(req.body);
        await task
            .save()
            .then(response => {
                return res.status(200).json(response);
            })
            .catch(error => {
                return res.status(500).json(error);
            })
    }

    async findById(req, res) {
        await BookModel.findOne({ '_id': req.params.id })
            .sort('when')
            .then(response => {
                return res.status(200).json(response);
            })
            .catch(err => {
                return res.status(500).json(err);
            })
    }

    async findAll(req, res) {
        await BookModel.find()
            .sort('when')
            .then(response => {
                return res.status(200).json(response);
            })
            .catch(err => {
                return res.status(500).json(err);
            })
    }

    async update(req, res) {
        await BookModel.findByIdAndUpdate(req.params.id, req.body, { new: true })
            .then(response => {
                return res.status(200).json(response);
            })
            .catch(err => {
                return res.status(500).json(err)
            })

    }

    async delete(req, res) {
        await BookModel.deleteOne({ '_id': req.params.id })
            .then(response => {
                return res.status(200).json(response);
            })
            .catch(err => {
                return res.status(500).json(err);
            })
    }
}

module.exports = new BookController();