const RateModel = require('../models/RateModel');

class RateController {

    async create(req, res) {
        const task = new RateModel(req.body);
        await task
            .save()
            .then(response => {
                return res.status(200).json(response);
            })
            .catch(error => {
                return res.status(500).json(error);
            })
    }

    async update(req, res) {
        await RateModel.findByIdAndUpdate(req.params.id, req.body, { new: true })
            .then(response => {
                return res.status(200).json(response);
            })
            .catch(err => {
                return res.status(500).json(err)
            })
    }

    async findByUserId(req, res) {
        await RateModel.find({ 'user_id': req.params.id })
            .sort('when')
            .then(response => {
                return res.status(200).json(response);
            })
            .catch(err => {
                return res.status(500).json(err);
            })
    }

    async findByBookId(req, res) {
        await RateModel.find({ 'book_id': req.params.id })
            .sort('when')
            .then(response => {
                return res.status(200).json(response);
            })
            .catch(err => {
                return res.status(500).json(err);
            })
    }
}

module.exports = new RateController();